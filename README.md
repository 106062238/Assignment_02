# AS_02_Raiden

## Complete game process
    * start menu
    * game view
    * game over
    * quit and play again

## Basic rules
* **player**
    * move
    * shoot
    * life decrease
* **enemy**
    * generate enemies
    * move
    * attack
* **map**
    * background move

## Jucify mechanisms
    * 5 levels depending on score
    * player ultimate skill

## Animations
    * player
    * enemy
    * UI
    * explosion

## Parcticle Systems
    * particle system when player or enemies are hit

## Sound effects
    * bgm
    * player shoot
    * enemy shoot
    * player explode
    * enemy explode
    * player ultimate alarm
    * player ultimate launch
    * player upgrade
    * can change volume

## UI
    * player health
    * ultimate skill energy bar
    * score
    * volume control
    * game pause
    * dynamic UI

## Leaderboard
    * firebase realtime database
    * show ladder
    * upload score

## Bonus
* **items**
    * heal
    * increase bullet
    * different weapons
* **enemy diffculty**
* **option page**
    * volume control
    * FPS
* **restart**
* **hotkeys**
    * ESC : menu
    * P : pause
    * M : mute
    * R : restart

var Menu = {};

Menu.preload = function() {};

Menu.create = function() {
    this.background = game.add.tileSprite(0, 0, 800, 600, 'background');
    this.background.tilePosition.y = game.global.backgroundPosition;
    this.titleLabel = initLabels(game.width / 2, 150, 'Raiden', {font: '60px Arial', fill: '#ffffff'}, 4.0, 3.0);
    this.titleLabel.tween = game.add.tween(this.titleLabel.scale).to({x: 1.0, y: 1.0}, 800).start();
    this.titleLabel.tween.onComplete.add(this.buttonTween, this);

    this.startButton = initButtons(game.width / 2, 290, 'menubutton', 0.0, 0.0, this.guiClicked, this);
    this.startButtonLabel = initLabels(this.startButton.x, this.startButton.y, 'Play', {font: '30px Arial', fill: '#ffffff'}, 0.0, 0.0);
    this.optionButton = initButtons(this.startButton.x, this.startButton.y + 80, 'menubutton', 0.0, 0.0, this.guiClicked, this);
    this.optionButtonLabel = initLabels(this.optionButton.x, this.optionButton.y, 'Option', {font: '30px Arial', fill: '#ffffff'}, 0.0, 0.0);
    this.leaderBoardButton = initButtons(this.optionButton.x, this.optionButton.y + 80, 'menubutton', 0.0, 0.0, this.guiClicked, this);
    this.leaderBoardButtonLabel = initLabels(this.leaderBoardButton.x, this.leaderBoardButton.y, 'LeaderBoard', {font: '30px Arial', fill: '#ffffff'}, 0.0, 0.0);

    this.soundButton = initButtons(game.width - 30, 30, 'sound', 0.6, 0.6, this.guiClicked, this);
    this.muteButton = initButtons(this.soundButton.x, this.soundButton.y, 'mute', 0.6, 0.6, this.guiClicked, this);
    this.soundButton.visible = !game.global.backgroundMusic.mute;
    this.muteButton.visible = game.global.backgroundMusic.mute;
};

Menu.update = function() {
    this.background.tilePosition.y += 0.5;
};

Menu.render = function() {
    if (game.global.displayFPS)
        game.debug.text(`FPS: ${game.time.fps}` || 'FPS: --', 2, 14, "#00ff00");
};

Menu.guiClicked = function(button) {
    switch (button) {
        case this.startButton:
            this.startGame();
            break;
        case this.optionButton:
            this.setOption();
            break;
        case this.leaderBoardButton:
            this.checkLeaderBoard();
            break;
        case this.soundButton:
            this.muteSound();
            break;
        case this.muteButton:
            this.muteSound();
            break;
        default:
            break;
    }
};

Menu.buttonTween = function() {
    this.startButton.tween = game.add.tween(this.startButton.scale).to({x: 1.8, y: 1.0}, 100).start();
    this.startButtonLabel.tween = game.add.tween(this.startButtonLabel.scale).to({x: 1.0, y: 1.0}, 100).start();
    this.optionButton.tween = game.add.tween(this.optionButton.scale).to({x: 1.8, y: 1.0}, 100).start();
    this.optionButtonLabel.tween = game.add.tween(this.optionButtonLabel.scale).to({x: 1.0, y: 1.0}, 100).start();
    this.leaderBoardButton.tween = game.add.tween(this.leaderBoardButton.scale).to({x: 1.8, y: 1.0}, 100).start();
    this.leaderBoardButtonLabel.tween = game.add.tween(this.leaderBoardButtonLabel.scale).to({x: 1.0, y: 1.0}, 100).start();
};

Menu.startGame = function() {
    game.global.backgroundPosition = this.background.tilePosition.y;
    game.state.start('Game');
};

Menu.setOption = function() {
    game.global.backgroundPosition = this.background.tilePosition.y;
    game.state.start('Option');
};

Menu.checkLeaderBoard = function() {
    game.global.backgroundPosition = this.background.tilePosition.y;
    game.state.start('LeaderBoard');
};

Menu.muteSound = function() {
    game.global.backgroundMusic.mute = !game.global.backgroundMusic.mute;
    this.soundButton.visible = !game.global.backgroundMusic.mute;
    this.muteButton.visible = game.global.backgroundMusic.mute;
};

function initButtons(x, y, tag, scaleX, scaleY, callback, user) {
    let button = game.add.button(x, y, tag, callback, user, GUI.hover, GUI.normal, GUI.clicked);
    button.scale.setTo(scaleX, scaleY);
    button.anchor.setTo(0.5, 0.5);
    return button;
};

function initLabels(x, y, text, style, scaleX, scaleY) {
    let label = game.add.text(x, y, text, style);
    label.scale.setTo(scaleX, scaleY);
    label.anchor.setTo(0.5, 0.5);
    return label;
};

function initImages(x, y, tag, scaleX, scaleY) {
    let image = game.add.image(x, y, tag);
    image.scale.setTo(scaleX, scaleY);
    image.anchor.setTo(0.5, 0.5);
    return image;
};
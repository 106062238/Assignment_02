// GUI frame definition

const GUI = {
    normal: 0,
    hover: 1,
    clicked: 2,
    locked: 3
};

// difficulty data

const DIFFCULTY = {
    'rookie': {
        tag: 'rookie',
        requirePoint: 0,
        nextRequirePoint: 100,
        maxEnemies: 4,
        enemySpeedX: 0.8,
        enemySpeedY: 0.6,
        enemyHealth: 0.5,
        enemyPoint: 0.5,
        enemyBullets: 1
    },
    'easy': {
        tag: 'easy',
        requirePoint: 100,
        nextRequirePoint: 1000,
        maxEnemies: 4,
        enemySpeedX: 1,
        enemySpeedY: 1,
        enemyHealth: 1,
        enemyPoint: 1,
        enemyBullets: 1
    },
    'hard': {
        tag: 'hard',
        requirePoint: 1000,
        nextRequirePoint: 2500,
        maxEnemies: 5,
        enemySpeedX: 1.2,
        enemySpeedY: 1.1,
        enemyHealth: 2.5,
        enemyPoint: 1.5,
        enemyBullets: 3
    },
    'nightmare': {
        tag: 'nightmare',
        requirePoint: 2500,
        nextRequirePoint: 10000,
        maxEnemies: 6,
        enemySpeedX: 1.3,
        enemySpeedY: 1.2,
        enemyHealth: 4.0,
        enemyPoint: 3.5,
        enemyBullets: 5
    },
    'hellrage': {
        tag: 'hellrage',
        requirePoint: 10000,
        nextRequirePoint: 10000000,
        maxEnemies: 9,
        enemySpeedX: 1.3,
        enemySpeedY: 1.2,
        enemyHealth: 8.0,
        enemyPoint: 7.0,
        enemyBullets: 7
    }
};

// weapon data

const WEAPONS = {
    'ammo': {
        tag: 'ammo',
        speed: 400,
        enemySpeed: 220,
        damage: 1,
        delay: 10,
        enemyDelay: 45
    },
    'burst': {
        tag: 'burst',
        speed: 380,
        enemySpeed: 160,
        damage: 2,
        delay: 12,
        enemyDelay: 90
    },
    'superhot': {
        tag: 'superhot',
        speed: 360,
        enemySpeed: 130,
        damage: 4,
        delay: 15,
        enemyDelay: 110
    },
    'bomb': {
        tag: 'bomb',
        speed: 300,
        enemySpeed: 200,
        damage: 10000,
        delay: 3600,
        enemyDelay: 300
    }
};

// player data

const PLAYER = {
    initialFrame: 5,
    health: 5,
    maxHealth: 10,
    weapon: WEAPONS.ammo
};

//enemy data

const ENEMIES = {
    'crazyivan': {
        scaleX: 1.0,
        scaleY: -1.0,
        speedX: 100,
        speedY: 60,
        health: 10,
        point: 10,
        weapon: WEAPONS.ammo
    },
    'stateczek': {
        scaleX: 1.0,
        scaleY: -1.0,
        speedX: 60,
        speedY: 40,
        health: 15,
        point: 50,
        weapon: WEAPONS.burst
    },
    'gepard': {
        scaleX: 0.3,
        scaleY: -0.3,
        speedX: 30,
        speedY: 10,
        health: 20,
        point: 100,
        weapon: WEAPONS.superhot
    }
};
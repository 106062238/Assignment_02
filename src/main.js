var game = new Phaser.Game(800, 600, Phaser.AUTO, document.getElementById('game'));
game.global = {
    backgroundPosition: 0,
    backgroundMusic: null,
    effectVolume: 1,
    displayFPS: true
};
game.state.add('Boot', Boot);
game.state.add('Load', Load);
game.state.add('Menu', Menu);
game.state.add('Option', Option);
game.state.add('LeaderBoard', LeaderBoard);
game.state.add('Game', Game);
game.state.start('Boot');
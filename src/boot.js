var Boot = {};

Boot.init = function() {
    game.stage.backgroundColor = '#000000';
    game.scale.pageAlignHorizontally = true;
    game.scale.pageAlignVertically = true;
    game.scale.refresh();
};

Boot.preload = function() {
    game.load.image('progressbar', 'src/assets/sprite/progressbar.png');
};

Boot.create = function() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.renderer.renderSession.roundPixels = true;
    game.state.start('Load');
};

Boot.update = function() {};
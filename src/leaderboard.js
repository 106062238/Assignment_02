var LeaderBoard = {};

LeaderBoard.preload = function() {};

LeaderBoard.create = function() {
    this.background = game.add.tileSprite(0, 0, 800, 600, 'background');
    this.background.tilePosition.y = game.global.backgroundPosition;
    this.titleLabel = initLabels(game.width / 2, 150, 'Leader Board', {font: '60px Arial', fill: '#ffffff'}, 1.0, 1.0);

    this.homeButton = initButtons(60, 60, 'home', 1.0, 1.0, this.guiClicked, this);
    
    this.soundButton = initButtons(game.width - 30, 30, 'sound', 0.6, 0.6, this.guiClicked, this);
    this.muteButton = initButtons(this.soundButton.x, this.soundButton.y, 'mute', 0.6, 0.6, this.guiClicked, this);
    this.soundButton.visible = !game.global.backgroundMusic.mute;
    this.muteButton.visible = game.global.backgroundMusic.mute;

    this.ladder = [];

    let ref = firebase.database().ref();
    ref.once('value')
        .then(snapshot => {
            snapshot.forEach(childSnapShot => {
                let rank = childSnapShot.key;
                let data = childSnapShot.val();
                let name = data.name;
                let score = data.score;

                this.ladder[rank - 1] = {};
                this.ladder[rank - 1].rank = initLabels(this.titleLabel.x - 250, this.titleLabel.y + 80 * rank, `${rank}.`, {font: '40px Arial', fill: '#ffffff'}, 1.0, 1.0);
                this.ladder[rank - 1].name = initLabels(this.ladder[rank - 1].rank.x + 50, this.ladder[rank - 1].rank.y, `${name}`, {font: '40px Arial', fill: '#ffffff'}, 1.0, 1.0);
                this.ladder[rank - 1].score = initLabels(this.ladder[rank - 1].name.x + 450, this.ladder[rank - 1].name.y, `${score}`, {font: '40px Arial', fill: '#ffffff'}, 1.0, 1.0);

                this.ladder[rank - 1].name.anchor.x = 0;
                this.ladder[rank - 1].score.anchor.x = 1;
            });
        })
        .catch(e => console.log(e.message));
};

LeaderBoard.update = function() {
    this.background.tilePosition.y += 0.5;
};

LeaderBoard.render = function() {
    if (game.global.displayFPS)
        game.debug.text(`FPS: ${game.time.fps}` || 'FPS: --', 2, 14, "#00ff00");
};

LeaderBoard.guiClicked = function(button) {
    switch (button) {
        case this.homeButton:
            this.goback();
            break;
        case this.soundButton:
            this.muteSound();
            break;
        case this.muteButton:
            this.muteSound();
            break;
        default:
            break;
    }
};

LeaderBoard.goback = function() {
    game.global.backgroundPosition = this.background.tilePosition.y;
    game.state.start('Menu');
};

LeaderBoard.muteSound = function() {
    game.global.backgroundMusic.mute = !game.global.backgroundMusic.mute;
    this.soundButton.visible = !game.global.backgroundMusic.mute;
    this.muteButton.visible = game.global.backgroundMusic.mute;
};
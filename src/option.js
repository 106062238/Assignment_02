var Option = {};

Option.preload = function() {};

Option.create = function() {
    this.background = game.add.tileSprite(0, 0, 800, 600, 'background');
    this.background.tilePosition.y = game.global.backgroundPosition;
    this.titleLabel = initLabels(game.width / 2, 150, 'Option', {font: '60px Arial', fill: '#ffffff'}, 1.0, 1.0);

    this.homeButton = initButtons(60, 60, 'home', 1.0, 1.0, this.guiClicked, this);
    
    this.soundButton = initButtons(game.width - 30, 30, 'sound', 0.6, 0.6, this.guiClicked, this);
    this.muteButton = initButtons(this.soundButton.x, this.soundButton.y, 'mute', 0.6, 0.6, this.guiClicked, this);
    this.soundButton.visible = !game.global.backgroundMusic.mute;
    this.muteButton.visible = game.global.backgroundMusic.mute;

    this.mainVolumeLabel = initLabels(this.titleLabel.x / 2, this.titleLabel.y + 100, 'main volume', {font: '30px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.mainVolumeBar = initSliderBars(this.mainVolumeLabel.x, this.mainVolumeLabel.y + 40, 0.5, 0.4);
    this.mainVolume = initSliders(0, this.mainVolumeLabel.y + 40, 2.0, 2.0, game.sound.volume, this.mainVolumeBar, this.adjustVolume, this);
    this.mainVolumeMinLabel = initLabels(this.mainVolumeBar.left - 30, this.mainVolumeBar.y + 25, '0', {font: '16px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.mainVolumeMaxLabel = initLabels(this.mainVolumeBar.right + 35, this.mainVolumeBar.y + 25, '100', {font: '16px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.mainVolumeValue = initLabels(this.mainVolume.x, this.mainVolumeBar.y + 25, `${(game.sound.volume * 100).toFixed(0)}`, {font: '10px Arial', fill: '#ffffff'}, 1.0, 1.0);

    this.bgmVolumeLabel = initLabels(this.mainVolumeLabel.x, this.mainVolumeLabel.y + 100, 'bgm volume', {font: '30px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.bgmVolumeBar = initSliderBars(this.bgmVolumeLabel.x, this.bgmVolumeLabel.y + 40, 0.5, 0.4);
    this.bgmVolume = initSliders(0, this.bgmVolumeLabel.y + 40, 2.0, 2.0, game.global.backgroundMusic.volume, this.bgmVolumeBar, this.adjustVolume, this);
    this.bgmVolumeMinLabel = initLabels(this.bgmVolumeBar.left - 30, this.bgmVolumeBar.y + 25, '0', {font: '16px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.bgmVolumeMaxLabel = initLabels(this.bgmVolumeBar.right + 35, this.bgmVolumeBar.y + 25, '100', {font: '16px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.bgmVolumeValue = initLabels(this.bgmVolume.x, this.bgmVolumeBar.y + 25, `${(game.global.backgroundMusic.volume * 100).toFixed(0)}`, {font: '10px Arial', fill: '#ffffff'}, 1.0, 1.0);
    
    this.effectVolumeLabel = initLabels(this.bgmVolumeLabel.x, this.bgmVolumeLabel.y + 100, 'effect volume', {font: '30px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.effectVolumeBar = initSliderBars(this.effectVolumeLabel.x, this.effectVolumeLabel.y + 40, 0.5, 0.4);
    this.effectVolume = initSliders(0, this.effectVolumeLabel.y + 40, 2.0, 2.0, game.global.effectVolume, this.effectVolumeBar, this.adjustVolume, this);
    this.effectVolumeMinLabel = initLabels(this.effectVolumeBar.left - 30, this.effectVolumeBar.y + 25, '0', {font: '16px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.effectVolumeMaxLabel = initLabels(this.effectVolumeBar.right + 35, this.effectVolumeBar.y + 25, '100', {font: '16px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.effectVolumeValue = initLabels(this.effectVolume.x, this.effectVolumeBar.y + 25, `${(game.global.effectVolume * 100).toFixed(0)}`, {font: '10px Arial', fill: '#ffffff'}, 1.0, 1.0);

    this.displayFPSLabel = initLabels(this.titleLabel.x + 100, this.mainVolumeLabel.y, 'display FPS:', {font: '30px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.FPSbutton = initButtons((this.displayFPSLabel.right + game.width) / 2, this.displayFPSLabel.y, 'menubutton', 1.0, 1.0, this.guiClicked, this);
    this.FPSLabel = initLabels(this.FPSbutton.x, this.FPSbutton.y, (game.global.displayFPS)? 'YES' : 'NO', {font: '30px Arial', fill: '#ffffff'}, 1.0, 1.0);
};

Option.update = function() {
    this.background.tilePosition.y += 0.5;
};

Option.render = function() {
    if (game.global.displayFPS)
        game.debug.text(`FPS: ${game.time.fps}` || 'FPS: --', 2, 14, "#00ff00");
    else
        game.debug.text();
};

Option.guiClicked = function(button) {
    switch (button) {
        case this.homeButton:
            this.goback();
            break;
        case this.soundButton:
            this.muteSound();
            break;
        case this.muteButton:
            this.muteSound();
            break;
        case this.FPSbutton:
            this.toggleFPS();
            break;
        default:
            break;
    }
};

Option.goback = function() {
    game.global.backgroundPosition = this.background.tilePosition.y;
    game.state.start('Menu');
};

Option.muteSound = function() {
    game.global.backgroundMusic.mute = !game.global.backgroundMusic.mute;
    this.soundButton.visible = !game.global.backgroundMusic.mute;
    this.muteButton.visible = game.global.backgroundMusic.mute;
};

Option.toggleFPS = function() {
    if (game.global.displayFPS)
        this.FPSLabel.text = 'NO';
    else
        this.FPSLabel.text = 'YES';
    game.global.displayFPS = !game.global.displayFPS;
};

Option.adjustVolume = function(slider) {
    switch (slider) {
        case this.mainVolume:
            game.sound.volume = (slider.x - slider.minX) / (slider.maxX - slider.minX);
            this.mainVolumeValue.text = `${(game.sound.volume * 100).toFixed(0)}`;
            this.mainVolumeValue.x = slider.x;
            break;
        case this.bgmVolume:
            game.global.backgroundMusic.volume = (slider.x - slider.minX) / (slider.maxX - slider.minX);
            this.bgmVolumeValue.text = `${(game.global.backgroundMusic.volume * 100).toFixed(0)}`;
            this.bgmVolumeValue.x = slider.x;
            break;
        case this.effectVolume:
            game.global.effectVolume = (slider.x - slider.minX) / (slider.maxX - slider.minX);
            this.effectVolumeValue.text = `${(game.global.effectVolume * 100).toFixed(0)}`;
            this.effectVolumeValue.x = slider.x;
            break;
        default:
            break;
    }
};

function initSliderBars(x, y, scaleX, scaleY) {
    let sliderbar = game.add.sprite(x, y, 'sliderbar');
    sliderbar.scale.setTo(scaleX, scaleY);
    sliderbar.anchor.setTo(0.5, 0.5);
    return sliderbar;
}

function initSliders(x, y, scaleX, scaleY, volume, bounds, callback, user) {
    let slider = game.add.sprite(x, y, 'slider');
    slider.scale.setTo(scaleX, scaleY);
    slider.anchor.setTo(0.5, 0.5);
    slider.maxX = bounds.x + bounds.width / 2 - slider.width / 2;
    slider.minX = bounds.x - bounds.width / 2 + slider.width / 2;
    slider.x = slider.minX + volume * (slider.maxX - slider.minX);
    slider.inputEnabled = true;
    slider.input.enableDrag(false, false, false, 255, null, bounds);
    slider.input.allowVerticalDrag = false;
    slider.events.onDragStop.add(callback, user);
    slider.events.onDragUpdate.add(callback, user);
    return slider;
}
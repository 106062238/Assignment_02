var Load = {};

Load.init = function() {
    
};

Load.preload = function() {
    game.time.advancedTiming = true;

    this.loadingLabel = initLabels(game.width / 2, 250, 'loading...', {font: '50px Arial', fill: '#ffffff'}, 1.0, 1.0);
    
    this.progressBar = game.add.sprite(game.width / 2, 350, 'progressbar');
    this.progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(this.progressBar);

    game.load.image('background', 'src/assets/sprite/background.png');
    game.load.spritesheet('menubutton', 'src/assets/sprite/gui/menubutton.png', 147, 64);

    game.load.image('sliderbar', 'src/assets/sprite/gui/sliderbar.png');
    game.load.image('slider', 'src/assets/sprite/gui/slider.png');

    game.load.spritesheet('player', 'src/assets/sprite/player/player.png', 26, 26);
    game.load.image('flame', 'src/assets/sprite/player/flame.png');

    game.load.image('ammo', 'src/assets/sprite/bullets/ammo.png');
    game.load.image('burst', 'src/assets/sprite/bullets/burst.png');
    game.load.image('superhot', 'src/assets/sprite/bullets/superhot.png');
    game.load.image('bomb', 'src/assets/sprite/bullets/bomb.png');
    game.load.image('particle', 'src/assets/sprite/bullets/particle.png');
    game.load.spritesheet('explosion', 'src/assets/sprite/bullets/explosion.png', 32, 32);

    game.load.image('crazyivan', 'src/assets/sprite/enemies/crazyivan.png');
    game.load.image('stateczek', 'src/assets/sprite/enemies/stateczek.png');
    game.load.image('gepard', 'src/assets/sprite/enemies/gepard.png');

    game.load.image('heart', 'src/assets/sprite/items/heart.png');
    game.load.image('ammoincrease', 'src/assets/sprite/items/ammoincrease.png');
    game.load.image('ammoburst', 'src/assets/sprite/items/ammoburst.png');
    game.load.image('ammosuperhot', 'src/assets/sprite/items/ammosuperhot.png');

    game.load.image('curtain', 'src/assets/sprite/gui/curtain.png');
    game.load.spritesheet('home', 'src/assets/sprite/gui/home.png', 65, 64);
    game.load.spritesheet('option', 'src/assets/sprite/gui/option.png', 64, 64);
    game.load.spritesheet('restart', 'src/assets/sprite/gui/restart.png', 64, 64);
    game.load.spritesheet('pause', 'src/assets/sprite/gui/pause.png', 64, 64);
    game.load.spritesheet('sound', 'src/assets/sprite/gui/sound.png', 65, 64);
    game.load.spritesheet('mute', 'src/assets/sprite/gui/mute.png', 65, 64);
    game.load.spritesheet('leader', 'src/assets/sprite/gui/leader.png', 65, 64);
    game.load.spritesheet('cancel', 'src/assets/sprite/gui/cancel.png', 64, 64);
    game.load.spritesheet('submit', 'src/assets/sprite/gui/submit.png', 64, 64);
    game.load.image('rightarrow', 'src/assets/sprite/gui/rightarrow.png');
    game.load.image('leftarrow', 'src/assets/sprite/gui/leftarrow.png');
    game.load.image('mainicon', 'src/assets/sprite/gui/mainicon.png');
    game.load.image('bgmicon', 'src/assets/sprite/gui/bgmicon.png');
    game.load.image('effecticon', 'src/assets/sprite/gui/effecticon.png');
    game.load.image('energybar', 'src/assets/sprite/gui/energybar.png');
    game.load.image('energy', 'src/assets/sprite/gui/energy.png');

    game.load.audio('gamebgm', ['src/assets/sound/gamebgm.wav', 'src/assets/sound/gamebgm.mp3']);
    game.load.audio('playerfire', ['src/assets/sound/player-fire.wav', 'src/assets/sound/player-fire.mp3']);
    game.load.audio('playerexplode', ['src/assets/sound/player-explosion.wav', 'src/assets/sound/player-explosion.mp3']);
    game.load.audio('enemyfire', ['src/assets/sound/enemy-fire.wav', 'src/assets/sound/enemy-fire.mp3']);
    game.load.audio('enemyexplode', ['src/assets/sound/enemy-explosion.wav', 'src/assets/sound/enemy-explosion.mp3']);
    game.load.audio('alarm', ['src/assets/sound/alarm.wav', 'src/assets/sound/alarm.mp3']);
    game.load.audio('launch', ['src/assets/sound/launch.wav', 'src/assets/sound/launch.mp3']);
    game.load.audio('upgrade', ['src/assets/sound/upgrade.wav', 'src/assets/sound/upgrade.mp3']);
};

Load.create = function() {
    game.global.backgroundMusic = game.add.audio('gamebgm');
    game.global.backgroundMusic.loop = true;
    game.global.backgroundMusic.play();
    game.state.start('Menu');
};

Load.update = function() {};
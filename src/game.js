var Game = {};

Game.preload = function() {};

Game.create = function() {
    this.background = game.add.tileSprite(0, 0, 800, 600, 'background');
    this.background.tilePosition.y = game.global.backgroundPosition;
    this.diffculty = DIFFCULTY.rookie;
    this.uploading = false;
    this.submitted = false;
    this.initPlayer();
    this.initEnemies();
    this.initExplosion();
    this.initParticle();
    this.initItems();
    this.initUI();
    this.initAudio();
    this.initInput();
};

Game.update = function() {
    this.background.tilePosition.y += 2;
    
    if (this.player.alive) {
        this.movePlayer();
        this.player.attack();
    }
    this.player.updateUltimate();
    this.enemies.fire();

    game.physics.arcade.overlap(this.player.bulletPool, this.enemies, this.enemyHit, null, this);
    if (this.player.alive) {
        game.physics.arcade.overlap(this.player, this.items, this.getItem, null, this);
        if (!this.player.immortal) {
            game.physics.arcade.overlap(this.player, this.enemies, this.playerCrashed, null, this);
            this.enemies.children.forEach(enemy => {
                game.physics.arcade.overlap(this.player, enemy.bulletPool, this.playerHit, null, this);
            });
        } else {
            this.player.updateImmortal();
        }
    }
};

Game.render = function() {
    if (game.global.displayFPS)
        game.debug.text(`FPS: ${game.time.fps}` || 'FPS: --', game.width - 74, 14, "#00ff00");
};

Game.initPlayer = function() {
    this.player = game.add.sprite(game.width / 2, 510, 'player');
    this.flame = {
        left: game.add.sprite(this.player.x - 10, this.player.bottom + 12, 'flame'),
        right: game.add.sprite(this.player.x + 10, this.player.bottom + 12, 'flame'),
        show: function() {
            this.left.x = Game.player.x - 10;
            this.left.y = Game.player.bottom + 12;
            this.right.x = Game.player.x + 10;
            this.right.y = Game.player.bottom + 12;
            this.left.visible = true;
            this.right.visible = true;
        },
        hide: function() {
            this.left.visible = false;
            this.right.visible = false;
        },
        init: function() {
            this.left.anchor.setTo(0.5, 0.5);
            this.right.anchor.setTo(0.5, 0.5);
            this.hide();
        }
    };
    this.flame.init();

    game.physics.arcade.enable(this.player);
    this.player.animations.add('leftturn', [5, 6, 7, 8, 9, 10], 30, false);
    this.player.animations.add('rightturn', [5, 4, 3, 2, 1, 0], 30, false);
    this.player.body.collideWorldBounds = true;
    this.player.body.immovable = true;

    this.player.bulletPool = game.add.group();
    this.player.bulletPool.enableBody = true;
    this.player.bulletPool.createMultiple(100, Object.keys(WEAPONS));
    this.player.bulletPool.setAll('anchor', {x: 0.5, y: 0.5});
    this.player.bulletPool.setAll('checkWorldBounds', true);
    this.player.bulletPool.setAll('outOfBoundsKill', true);
    
    let bombs = this.player.bulletPool.getAll('key', 'bomb', 300, 400);
    bombs.forEach(bomb => {
        bomb.scale.setTo(-1.5, 1.5);
        bomb.speed = WEAPONS.bomb.speed;
        bomb.turnRate = 5;
        bomb.attackDamage = WEAPONS.bomb.damage;
    });

    this.player.dataReset = function() {
        this.frame = PLAYER.initialFrame;
        this.scale.setTo(2, 2);
        this.anchor.setTo(0.5, 0.5);
        this.reset(game.width / 2, 510);

        this.immortal = false;
        this.immortalCount = 0;
        if (this.flash)
            this.flash.stop();
        this.visible = true;
        this.health = PLAYER.health;
        this.maxHealth = PLAYER.maxHealth;

        this.ultimateCooling = false;
        this.ultimateCD = WEAPONS.bomb.delay;
        this.ultimateCount = 0;
        this.weapon = PLAYER.weapon;
        this.attackCount = 0;
        this.playerBullets = 1;

        let bullets = this.bulletPool.getAll();
        bullets.forEach (bullet => {
            bullet.kill();
        });
    };
    this.player.dataReset();

    this.player.updateImmortal = function() {
        if (++this.immortalCount >= 90) {
            this.immortal = false;
            this.immortalCount = 0;
            this.flash.stop();
            this.visible = true;
        }
    };

    this.player.attack = function() {
        if (++this.attackCount >= this.weapon.delay) {
            let searchRange = bulletPoolSearchRange(this.weapon.tag);
            let bullets = this.bulletPool.getAll('alive', false, searchRange.start, searchRange.end);
            let half = Math.floor(this.playerBullets / 2);
            for (let i = -half; i <= half; i++) {
                let bullet = bullets.shift();
                bullet.reset(this.x, this.body.top);
                bullet.body.velocity.y = -this.weapon.speed * Math.cos(i * Math.PI / 12);
                bullet.body.velocity.x = this.weapon.speed * Math.sin(i * Math.PI / 12);
                bullet.attackDamage = this.weapon.damage;
            }
            this.attackCount = 0;
            this.attackSound.play();
        }
    };

    this.player.ultimate = function() {
        if (!game.paused && this.player.alive && !this.player.ultimateCooling) {
            this.player.ultimateCooling = true;
            Game.energyLabel.flash.stop();
            Game.energyLabel.visible = false;
            game.camera.shake(0.02, 1200);
            this.alarm.play();
            let bombs = Game.player.bulletPool.getAll('alive', false, 300, 310);
            game.time.events.add(1200, () => {
                if (this.player.alive) {
                    this.enemies.children.forEach(enemy => {
                        if (enemy.alive) {
                            let bomb = bombs.shift();
                            bomb.reset(Game.player.x, Game.player.top);
                            bomb.lifespan = 10000;
                            bomb.target = enemy;
                        }
                    });
                    this.launch.play();
                }
            }, this);
        }
    };

    this.player.updateUltimate = function() {
        if (this.ultimateCooling) {
            if (++this.ultimateCount >= this.ultimateCD) {
                this.ultimateCooling = false;
                this.ultimateCount = 0;
                Game.energyLabel.visible = true;
                Game.energyLabel.alpha = 1;
                Game.energyLabel.flash = game.add.tween(Game.energyLabel).to({alpha: 0}, 400).yoyo(true).loop(true).start();
            }
            Game.energy.ratio = (Game.player.ultimateCount * Game.player.ultimateCooling + Game.player.ultimateCD * !Game.player.ultimateCooling) / Game.player.ultimateCD;
            Game.energy.scale.x = Game.energy.ratio;
        }
        
        let bombs = this.bulletPool.getAll('alive', true, 300, 400);
        while (bombs.length) {
            let bomb = bombs.shift();
            bulletTrace(bomb);
        }
    };
};

Game.initEnemies = function() {
    this.enemies = game.add.group();
    this.enemies.enableBody = true;
    this.enemies.createMultiple(3, Object.keys(ENEMIES));
    this.enemies.setAll('anchor', {x: 0.5, y: 0.5});
    this.enemies.setAll('body.immovable', true);
    this.enemies.setAll('checkWorldBounds', true);

    this.enemies.children.forEach(enemy => {
        enemy.attackCount = 0;
        enemy.bulletPool = game.add.group();
        enemy.bulletPool.enableBody = true;
        enemy.bulletPool.createMultiple(100, Object.keys(WEAPONS));
        enemy.bulletPool.setAll('anchor', {x: 0.5, y: 0.5});
        enemy.bulletPool.setAll('checkWorldBounds', true);
        enemy.bulletPool.setAll('outOfBoundsKill', true);

        enemy.dataReset = function() {
            let bullets = this.bulletPool.getAll();
            bullets.forEach (bullet => {
                bullet.kill();
            });
            enemy.kill();
        };

        enemy.attack = function() {
            if (++this.attackCount >= this.weapon.enemyDelay) {
                let searchRange = bulletPoolSearchRange(this.weapon.tag);
                let bullets = this.bulletPool.getAll('alive', false, searchRange.start, searchRange.end);
                let half = Math.floor(Game.diffculty.enemyBullets / 2);
                for (let i = -half; i <= half; i++) {
                    let bullet = bullets.shift();
                    bullet.reset(this.x, this.body.bottom);
                    bullet.body.velocity.y = this.weapon.enemySpeed * Math.cos(i * Math.PI / 6);
                    bullet.body.velocity.x = this.weapon.enemySpeed * Math.sin(i * Math.PI / 6);
                    bullet.attackDamage = this.weapon.damage;
                }
                this.attackCount = 0;
                Game.enemies.attackSound.play();
            }
        };
    });

    this.enemies.fire = function() {
        this.children.forEach(enemy => {
            if (enemy.alive)
                enemy.attack();
        });
    };

    this.enemies.dataReset = function() {
        this.children.forEach(enemy => {
            enemy.dataReset();
        });
    };

    game.time.events.loop(3000, this.addEnemy, this);
};

Game.initExplosion = function() {
    this.explosions = game.add.group();
    this.explosions.createMultiple(50, 'explosion');
    this.explosions.setAll('anchor', {x: 0.5, y: 0.5});
    this.explosions.children.forEach(explosion => {
        explosion.animations.add('boom');
    });

    this.explosions.dataReset = function() {
        let explosions = this.getAll();
        explosions.forEach(explosion => {
            explosion.kill();
        });
    };
};

Game.initParticle = function() {
    this.emitters = game.add.group();
    for (let i = 0; i < 50; i++) {
        let emitter = game.add.emitter(0, 0, 5);
        this.emitters.add(emitter);
        emitter.makeParticles('particle');
        emitter.setYSpeed(-100, 100);
        emitter.setXSpeed(-100, 100);
        emitter.setScale(1, 0, 1, 0, 800);
        emitter.gravity = 0;
        emitter.kill();
    }

    this.emitters.dataReset = function() {
        let emitters = this.getAll();
        emitters.forEach(emitter => {
            emitter.kill();
        });
    };
};

Game.initItems = function() {
    this.items = game.add.group();
    this.items.enableBody = true;
    this.items.createMultiple(10, ['heart', 'ammoincrease', 'ammoburst', 'ammosuperhot']);
    this.items.setAll('anchor', {x: 0.5, y: 0.5});
    this.items.setAll('lifespan', 20000);
    this.items.children.forEach(item => {
        if (item.key === 'heart')
            item.scale.setTo(0.25, 0.25);
        else if (item.key === 'ammoincrease')
            item.scale.setTo(0.07, 0.07);
        else if (item.key === 'ammoburst')
            item.scale.setTo(0.05, 0.05);
        else if (item.key === 'ammosuperhot')
            item.scale.setTo(0.05, 0.05);
    });

    this.items.dataReset = function() {
        let items = this.getAll();
        items.forEach(item => {
            item.kill();
        });
    };
};

Game.initUI = function() {
    this.score = 0;
    this.scoreLabel = game.add.text(30, 30, `Score: ${this.score}`, {font: '18px Arial', fill: '#ffffff'});
    this.healthLabel = game.add.text(30, 60, `Health: ${this.player.health}`, {font: '18px Arial', fill: '#ffffff'});

    this.curtain = initImages(-15, game.height / 2, 'curtain', 1.0, 1.0);
    this.homeButton = initButtons(this.curtain.x - 5, this.curtain.top + 40, 'home', 0.6, 0.6, this.guiClicked, this);
    this.optionButton = initButtons(this.homeButton.x, this.homeButton.bottom + 30, 'option', 0.6, 0.6, this.guiClicked, this);
    this.restartButton = initButtons(this.optionButton.x, this.optionButton.bottom + 30, 'restart', 0.6, 0.6, this.guiClicked, this);
    this.pauseButton = initButtons(this.restartButton.x, this.restartButton.bottom + 30, 'pause', 0.6, 0.6, this.guiClicked, this);
    this.soundButton = initButtons(this.pauseButton.x, this.pauseButton.bottom + 30, 'sound', 0.6, 0.6, this.guiClicked, this);
    this.muteButton = initButtons(this.soundButton.x, this.soundButton.y, 'mute', 0.6, 0.6, this.guiClicked, this);
    this.soundButton.visible = !game.global.backgroundMusic.mute;
    this.muteButton.visible = game.global.backgroundMusic.mute;
    this.rightArrow = initImages(this.curtain.x + 25, this.curtain.y, 'rightarrow', 0.7, 0.7);
    this.rightArrow.inputEnabled = true;
    this.rightArrow.events.onInputDown.add(this.toggleUI, this);
    this.leftArrow = initImages(this.rightArrow.x, this.rightArrow.y, 'leftarrow', 0.7, 0.7);
    this.leftArrow.inputEnabled = true;
    this.leftArrow.events.onInputDown.add(this.toggleUI, this);
    this.leftArrow.visible = false;

    this.soundCurtain = initImages(this.optionButton.x + 200, this.optionButton.y, 'curtain', 3.0, 0.4)
    this.mainicon = initImages(this.soundCurtain.x - 75, this.optionButton.y - 30, 'mainicon', 0.4, 0.4);
    this.mainVolumeBar = initSliderBars(this.soundCurtain.x + 20, this.mainicon.y, 0.3, 0.3);
    this.mainVolume = initSliders(0, this.mainVolumeBar.y, 1.5, 1.5, game.sound.volume, this.mainVolumeBar, this.adjustVolume, this);
    this.bgmicon = initImages(this.mainicon.x, this.optionButton.y, 'bgmicon', 0.4, 0.4);
    this.bgmVolumeBar = initSliderBars(this.mainVolumeBar.x, this.bgmicon.y, 0.3, 0.3);
    this.bgmVolume = initSliders(0, this.bgmVolumeBar.y, 1.5, 1.5, game.global.backgroundMusic.volume, this.bgmVolumeBar, this.adjustVolume, this);
    this.effecticon = initImages(this.mainicon.x, this.optionButton.y + 30, 'effecticon', 0.4, 0.4);
    this.effectVolumeBar = initSliderBars(this.bgmVolumeBar.x, this.effecticon.y, 0.3, 0.3);
    this.effectVolume = initSliders(0, this.effectVolumeBar.y, 1.5, 1.5, game.global.effectVolume, this.effectVolumeBar, this.adjustVolume, this);
    this.setVolumeControlVisibility(false);

    this.energyBar = initImages(70, game.height - 20, 'energybar', 1.0, 1.0);
    let minX = this.energyBar.left + 4;
    this.energy = initImages(minX, this.energyBar.y, 'energy', 1.0, 1.0);
    this.energy.anchor.x = 0;
    this.energy.ratio = (Game.player.ultimateCount * Game.player.ultimateCooling + Game.player.ultimateCD * !Game.player.ultimateCooling) / Game.player.ultimateCD;
    this.energy.scale.x = Game.energy.ratio;
    this.energyLabel = game.add.text(this.energyBar.x, this.energyBar.y, 'SPACE BAR', {font: '14px Arial', fill: '#ffffff'});
    this.energyLabel.anchor.setTo(0.5, 0.4);
    this.energyLabel.flash = game.add.tween(this.energyLabel).to({alpha: 0}, 400).yoyo(true).loop(true).start();
};

Game.guiClicked = function(button) {
    switch (button) {
        case this.homeButton:
            this.gameQuit();
            break;
        case this.optionButton:
            this.setVolumeControlVisibility(!this.soundCurtain.visible);
            break;
        case this.restartButton:
            this.reset();
            break;
        case this.pauseButton:
            this.pauseGame();
            break;
        case this.soundButton:
            this.muteSound();
            break;
        case this.muteButton:
            this.muteSound();
            break;
        case this.uploadButton:
            this.popTopScores();
            break;
        case this.cancelButton:
            this.closeTopScores();
            break;
        case this.submitButton:
            this.submitScore();
            break;
        default:
            break;
    }
};

Game.initAudio = function() {
    this.player.attackSound = game.add.audio('playerfire');
    this.player.explosionSound = game.add.audio('playerexplode');
    this.enemies.attackSound = game.add.audio('enemyfire');
    this.enemies.explosionSound = game.add.audio('enemyexplode');
    this.alarm = game.add.audio('alarm');
    this.launch = game.add.audio('launch');
    this.upgrade = game.add.audio('upgrade');
    this.setEffectVolume(game.global.effectVolume);
    this.onMute(true, game.global.backgroundMusic.mute);
};

Game.initInput = function() {
    this.cursor = game.input.keyboard.createCursorKeys();
    this.ultimateSkill = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    this.ultimateSkill.onDown.add(this.player.ultimate, this);
    this.escape = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
    this.escape.onDown.add(this.gameQuit, this);
    this.restart = game.input.keyboard.addKey(Phaser.Keyboard.R);
    this.restart.onDown.add(this.reset, this);
    this.pause = game.input.keyboard.addKey(Phaser.Keyboard.P);
    this.pause.onDown.add(this.pauseGame, this);
    this.mute = game.input.keyboard.addKey(Phaser.Keyboard.M);
    this.mute.onDown.add(this.muteSound, this);
    this.soundAdd = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_ADD);
    this.soundAdd.onDown.add(() => {
        if (!game.paused) {
            game.sound.volume = (game.sound.volume * 10 + 1) / 10;
            this.mainVolume.x = this.mainVolume.minX + game.sound.volume * (this.mainVolume.maxX - this.mainVolume.minX);
        }
    }, this);
    this.soundMinus = game.input.keyboard.addKey(Phaser.Keyboard.NUMPAD_SUBTRACT);
    this.soundMinus.onDown.add(() => {
        if (!game.paused) {
            game.sound.volume = (game.sound.volume * 10 - 1) / 10;
            this.mainVolume.x = this.mainVolume.minX + game.sound.volume * (this.mainVolume.maxX - this.mainVolume.minX);
        }
    }, this);

    this.keyA = game.input.keyboard.addKey(Phaser.Keyboard.A);
    this.keyB = game.input.keyboard.addKey(Phaser.Keyboard.B);
    this.keyC = game.input.keyboard.addKey(Phaser.Keyboard.C);
    this.keyD = game.input.keyboard.addKey(Phaser.Keyboard.D);
    this.keyE = game.input.keyboard.addKey(Phaser.Keyboard.E);
    this.keyF = game.input.keyboard.addKey(Phaser.Keyboard.F);
    this.keyG = game.input.keyboard.addKey(Phaser.Keyboard.G);
    this.keyH = game.input.keyboard.addKey(Phaser.Keyboard.H);
    this.keyI = game.input.keyboard.addKey(Phaser.Keyboard.I);
    this.keyJ = game.input.keyboard.addKey(Phaser.Keyboard.J);
    this.keyK = game.input.keyboard.addKey(Phaser.Keyboard.K);
    this.keyL = game.input.keyboard.addKey(Phaser.Keyboard.L);
    this.keyN = game.input.keyboard.addKey(Phaser.Keyboard.N);
    this.keyO = game.input.keyboard.addKey(Phaser.Keyboard.O);
    this.keyQ = game.input.keyboard.addKey(Phaser.Keyboard.Q);
    this.keyS = game.input.keyboard.addKey(Phaser.Keyboard.S);
    this.keyT = game.input.keyboard.addKey(Phaser.Keyboard.T);
    this.keyU = game.input.keyboard.addKey(Phaser.Keyboard.U);
    this.keyV = game.input.keyboard.addKey(Phaser.Keyboard.V);
    this.keyW = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.keyX = game.input.keyboard.addKey(Phaser.Keyboard.X);
    this.keyY = game.input.keyboard.addKey(Phaser.Keyboard.Y);
    this.keyZ = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    this.keyBackSpace = game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);
    this.keyA.onDown.add(this.updateName, this);
    this.keyB.onDown.add(this.updateName, this);
    this.keyC.onDown.add(this.updateName, this);
    this.keyD.onDown.add(this.updateName, this);
    this.keyE.onDown.add(this.updateName, this);
    this.keyF.onDown.add(this.updateName, this);
    this.keyG.onDown.add(this.updateName, this);
    this.keyH.onDown.add(this.updateName, this);
    this.keyI.onDown.add(this.updateName, this);
    this.keyJ.onDown.add(this.updateName, this);
    this.keyK.onDown.add(this.updateName, this);
    this.keyL.onDown.add(this.updateName, this);
    this.mute.onDown.add(this.updateName, this);
    this.keyN.onDown.add(this.updateName, this);
    this.keyO.onDown.add(this.updateName, this);
    this.pause.onDown.add(this.updateName, this);
    this.keyQ.onDown.add(this.updateName, this);
    this.restart.onDown.add(this.updateName, this);
    this.keyS.onDown.add(this.updateName, this);
    this.keyT.onDown.add(this.updateName, this);
    this.keyU.onDown.add(this.updateName, this);
    this.keyV.onDown.add(this.updateName, this);
    this.keyW.onDown.add(this.updateName, this);
    this.keyX.onDown.add(this.updateName, this);
    this.keyY.onDown.add(this.updateName, this);
    this.keyZ.onDown.add(this.updateName, this);
    this.keyBackSpace.onDown.add(this.updateName, this);

    // below is for testing
    this.testGameOver = game.input.keyboard.addKey(Phaser.Keyboard.F8);
    this.testGameOver.onDown.add(this.playerDie, this);
    this.testNextLevel = game.input.keyboard.addKey(Phaser.Keyboard.F7);
    this.testNextLevel.onDown.add(this.nextLevel, this);
};

Game.gameQuit = function() {
    if (!game.paused) {
        game.global.backgroundPosition = this.background.tilePosition.y;
        game.state.start('Menu');
    }
};

Game.addEnemy = function() {
    if (this.enemies.countLiving() >= this.diffculty.maxEnemies) return;
    let enemies = this.enemies.getAll('alive', false);
    let enemy = enemies[game.rnd.integerInRange(0, enemies.length - 1)];
    e = ENEMIES[`${enemy.key}`];
    enemy.scale.setTo(e.scaleX, e.scaleY);
    enemy.reset(game.world.randomX, enemy.height / 2 + 1);
    enemy.body.velocity.x = e.speedX * game.rnd.pick([-1, 1]) * this.diffculty.enemySpeedX;
    enemy.body.velocity.y = e.speedY * this.diffculty.enemySpeedY;
    enemy.events.onOutOfBounds.add(this.bounceEnemy, this);
    enemy.health = e.health * this.diffculty.enemyHealth;
    enemy.point = e.point * this.diffculty.enemyPoint;
    enemy.weapon = e.weapon;
};

Game.movePlayer = function() {
    if (this.cursor.left.isDown) {
        this.player.body.velocity.x = -250;
        if (this.player.frame != 10)
            this.player.animations.play('leftturn');
    } else if (this.cursor.right.isDown) { 
        this.player.body.velocity.x = 250;
        if (this.player.frame != 0)
            this.player.animations.play('rightturn');
    } else {
        this.player.body.velocity.x = 0;
        this.player.animations.stop();
        this.player.frame = PLAYER.initialFrame;
    }

    if (this.cursor.up.isDown) { 
        this.player.body.velocity.y = -200;
        this.flame.show();
    } else if (this.cursor.down.isDown) {
        this.player.body.velocity.y = 200;
        this.flame.hide();
    } else {
        this.player.body.velocity.y = 0;
        this.flame.hide();
    }
};

Game.bounceEnemy = function(enemy) {
    if (enemy.y > game.height - enemy.height / 2)
        enemy.kill();
    else
        enemy.body.velocity.x *= -1;
};

Game.getItem = function(player, item) {
    switch (item.key) {
        case 'heart':
            player.heal(game.rnd.integerInRange(1, 5));
            this.healthLabel.text = `Health: ${this.player.health}`;
            break;
        case 'ammoincrease':
            player.playerBullets = Math.min(5, player.playerBullets + 2);
            break;
        case 'ammoburst':
            player.weapon = WEAPONS.burst;
            break;
        case 'ammosuperhot':
            player.weapon = WEAPONS.superhot;
            break;
    }
    item.kill();
    this.upgrade.play();
};

Game.enemyHit = function(bullet, enemy) {
    this.bulletEffect(bullet);

    enemy.damage(bullet.attackDamage);
    bullet.kill();
    if (!enemy.alive) {
        if (this.player.alive)
            this.score += enemy.point;
        this.scoreLabel.text = `Score: ${this.score}`;
        this.unitExplosion(enemy);
        this.enemies.explosionSound.play();
        if (game.rnd.frac() < 0.25)
            this.generateItem(enemy);
        this.checkNextLevel();
    }
};

Game.playerCrashed = function() {
    if (!this.player.immortal) {
        this.player.damage(1);
        this.healthLabel.text = `Health: ${this.player.health}`;
        this.checkPlayerDead();
    };
};

Game.playerHit = function(player, bullet) {
    if (!player.immortal) {
        game.camera.shake(0.015, 300);
        this.bulletEffect(bullet);
        player.damage(bullet.attackDamage);
        bullet.kill();
        this.healthLabel.text = `Health: ${player.health}`;
        this.checkPlayerDead();
    }
};

Game.generateItem = function(enemy) {
    let number = game.rnd.frac();
    let array;
    let item;
    if (number < 0.4)
        array = this.items.getAll('alive', false, 0, 10);
    else if (number >= 0.4 && number < 0.55)
        array = this.items.getAll('alive', false, 10, 20);
    else if (number >= 0.55 && number < 0.8)
        array = this.items.getAll('alive', false, 20, 30);
    else
        array = this.items.getAll('alive', false, 30, 40);
    item = array.shift();
    item.reset(enemy.x, enemy.y);
    item.lifespan = 20000;
    let direction = game.rnd.normal();
    item.float = game.add.tween(item.body.velocity).to({x: 25 * direction, y: 20 * direction}, 3000).to({x: 0, y: 0}, 3000).to({x: -25 * direction, y: -20 * direction}, 6000).to({x: 0, y: 0}, 6000).loop(true).start();
};

Game.bulletEffect = function(bullet) {
    this.bulletExplosion(bullet);
    this.bulletParticle(bullet);
};

Game.bulletExplosion = function(bullet) {
    let explosion = this.explosions.getFirstDead();
    explosion.scale.setTo(1.3, 1.3);
    explosion.reset(bullet.x, bullet.y);
    explosion.play('boom', 15, false, true);
};

Game.bulletParticle = function(bullet) {
    let emitter = this.emitters.getFirstDead();
    emitter.x = bullet.x;
    emitter.y = bullet.y;
    emitter.start(true, 800, null, 15);
    game.time.events.add(1000, () => emitter.kill(), this);
};

Game.unitExplosion = function(unit) {
    let explosion = this.explosions.getFirstDead();
    explosion.scale.setTo(4.0, 4.0);
    explosion.reset(unit.x, unit.y);
    explosion.play('boom', 10, false, true);
};

Game.checkNextLevel = function() {
    if (this.score >= this.diffculty.nextRequirePoint)
        this.nextLevel();
};

Game.nextLevel = function() {
    if (this.diffculty.tag === 'rookie')
        this.diffculty = DIFFCULTY.easy;
    else if (this.diffculty.tag === 'easy')
        this.diffculty = DIFFCULTY.hard;
    else if (this.diffculty.tag === 'hard')
        this.diffculty = DIFFCULTY.nightmare;
    else if (this.diffculty.tag === 'nightmare')
        this.diffculty = DIFFCULTY.hellrage;
};

Game.checkPlayerDead = function() {
    if (this.player.alive) {
        this.player.immortal = true;
        this.player.flash = game.add.tween(this.player).to({visible: false}, 50).yoyo(true).loop(true).start();
    } else {
        this.unitExplosion(this.player);
        this.flame.hide();
        this.player.explosionSound.play();
        this.gameOver();
    }
};

Game.playerDie = function() {
    if (this.player.alive) {
        this.player.damage(this.player.health);
        this.healthLabel.text = `Health: ${this.player.health}`;
        this.unitExplosion(this.player);
        this.flame.hide();
        this.player.flash.stop();
        this.player.visible = false;
        this.player.explosionSound.play();
        this.gameOver();
    }
};

Game.gameOver = function() {
    this.showGameOverLabel();
};

Game.showGameOverLabel = function() {
    this.removeGameOverLabel();
    this.gameOverLabel = initLabels(game.width / 2, -50, 'Game Over!', {font: '50px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.gameOverLabel.fall = game.add.tween(this.gameOverLabel).to({y: game.height / 2 - 100}, 1000).easing(Phaser.Easing.Bounce.Out).start();
    this.resultScore = initLabels(this.gameOverLabel.x, game.height / 2, `Your Score: ${this.score}`, {font: '40px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.uploadButton = initButtons(this.resultScore.x, this.resultScore.y + 50, 'leader', 0.6, 0.6, this.guiClicked, this);
    this.restartHint = initLabels(this.resultScore.x, this.resultScore.y + 100, 'Press R to restart', {font: '30px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.restartHint.tween = game.add.tween(this.restartHint).to({y: game.height / 2 + 90}, 100).yoyo(true).loop(true).start();
    this.restartHint.tween.delay(1000);
    this.quitHint = initLabels(this.restartHint.x, this.restartHint.y + 50, 'Press ESC to quit', {font: '30px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.quitHint.tween = game.add.tween(this.quitHint).to({y: game.height / 2 + 140}, 100).yoyo(true).loop(true).start();
    this.quitHint.tween.delay(1000);
};

Game.removeGameOverLabel = function() {
    if (this.gameOverLabel && this.restartHint && this.quitHint) {
        this.gameOverLabel.fall.stop();
        this.gameOverLabel.destroy();
        this.resultScore.destroy();
        this.uploadButton.destroy();
        this.restartHint.tween.stop();
        this.restartHint.destroy();
        this.quitHint.tween.stop();
        this.quitHint.destroy();
    }
};

Game.popTopScores = function() {
    this.pauseGame();
    this.pauseButton.inputEnabled = false;
    this.uploadButton.inputEnabled = false;
    this.uploadMask = game.add.graphics(0, 0);
    this.uploadMask.beginFill(0x000000, 1.0);
    this.uploadMask.drawRect(0, 0, game.width, game.height);
    this.cancelButton = initButtons(60, 60, 'cancel', 1.0, 1.0, this.guiClicked, this);
    this.uploadScore = initLabels(game.width / 2, game.height / 2 - 100, `Your Score: ${this.score}`, {font: '60px Arial', fill: '#ffffff'}, 1.0, 1.0);
    this.checkLeaders();
};

Game.closeTopScores = function() {
    this.uploading = false;
    if (this.submitButton)
        this.submitButton.destroy();
    if (this.uploadName)
        this.uploadName.destroy();
    if (this.uploadInstruction)
        this.uploadInstruction.destroy();
    if (this.uploadMessage)
        this.uploadMessage.destroy();
    this.uploadScore.destroy();
    this.cancelButton.destroy();
    this.uploadMask.destroy();
    this.uploadButton.inputEnabled = true;
    this.pauseButton.inputEnabled = true;
    this.uploadButton.frame = GUI.normal;
    this.pauseGame();
};

Game.checkLeaders = function() {
    this.uploading = true;
    this.ref = firebase.database().ref();
    this.ref.once('value')
        .then(snapshot => {
            if (this.uploading) {
                this.leaders = snapshot.val();
                if (this.leaders)
                    this.leaders.shift();
                else
                    this.leaders = [];
                this.newData = {};
                this.newData.score = this.score;
                this.leaders.push(this.newData);
                this.leaders.sort((data1, data2) => {
                    return data2.score - data1.score;
                });
                if (this.leaders.indexOf(this.newData) < 5 && this.uploading) {
                    this.uploadMessage = initLabels(this.uploadScore.x, this.uploadScore.y + 100, 'Congratulations! Your score is in the top 5!', {font: '20px Arial', fill: '#ffffff'}, 1.0, 1.0);
                    this.uploadInstruction = initLabels(this.uploadMessage.x, this.uploadMessage.y + 50, 'Enter your name to upload your score!', {font: '20px Arial', fill: '#ffffff'}, 1.0, 1.0);
                    this.uploadName = initLabels(this.uploadInstruction.x, this.uploadInstruction.y + 50, '', {font: '20px Arial', fill: '#ffffff'}, 1.0, 1.0);
                    this.submitButton = initButtons(this.uploadName.x, this.uploadName.y + 80, 'submit', 1.0, 1.0, this.guiClicked, this);
                } else if (this.uploading) {
                    this.uploadMessage = initLabels(this.uploadScore.x, this.uploadScore.y + 100, 'Sorry! Your score is not in the top 5!', {font: '20px Arial', fill: '#ffffff'}, 1.0, 1.0);
                    this.uploadInstruction = initLabels(this.uploadMessage.x, this.uploadMessage.y + 50, 'Try harder next time!', {font: '20px Arial', fill: '#ffffff'}, 1.0, 1.0);
                }
            }
        })
        .catch(e => console.log(e.message));
};

Game.updateName = function() {
    if (this.uploadName) {
        if (this.uploadName.text.length < 10) {
            if (this.keyA.isDown)
                this.uploadName.text += 'A';
            else if (this.keyB.isDown)
                this.uploadName.text += 'B';
            else if (this.keyC.isDown)
                this.uploadName.text += 'C';
            else if (this.keyD.isDown)
                this.uploadName.text += 'D';
            else if (this.keyE.isDown)
                this.uploadName.text += 'E';
            else if (this.keyF.isDown)
                this.uploadName.text += 'F';
            else if (this.keyG.isDown)
                this.uploadName.text += 'G';
            else if (this.keyH.isDown)
                this.uploadName.text += 'H';
            else if (this.keyI.isDown)
                this.uploadName.text += 'I';
            else if (this.keyJ.isDown)
                this.uploadName.text += 'J';
            else if (this.keyK.isDown)
                this.uploadName.text += 'K';
            else if (this.keyL.isDown)
                this.uploadName.text += 'L';
            else if (this.mute.isDown)
                this.uploadName.text += 'M';
            else if (this.keyN.isDown)
                this.uploadName.text += 'N';
            else if (this.keyO.isDown)
                this.uploadName.text += 'O';
            else if (this.pause.isDown)
                this.uploadName.text += 'P';
            else if (this.keyQ.isDown)
                this.uploadName.text += 'Q';
            else if (this.restart.isDown)
                this.uploadName.text += 'R';
            else if (this.keyS.isDown)
                this.uploadName.text += 'S';
            else if (this.keyT.isDown)
                this.uploadName.text += 'T';
            else if (this.keyU.isDown)
                this.uploadName.text += 'U';
            else if (this.keyV.isDown)
                this.uploadName.text += 'V';
            else if (this.keyW.isDown)
                this.uploadName.text += 'W';
            else if (this.keyX.isDown)
                this.uploadName.text += 'X';
            else if (this.keyY.isDown)
                this.uploadName.text += 'Y';
            else if (this.keyZ.isDown)
                this.uploadName.text += 'Z';
        }
        if (this.keyBackSpace.isDown)
            this.uploadName.text = this.uploadName.text.slice(0, this.uploadName.text.length - 1);
    }
};

Game.submitScore = function() {
    if (!this.submitted) {
        this.submitted = true;
        this.newData.name = (this.uploadName.text)? this.uploadName.text : 'NONAME';
        let updates = {};
        for (let i = 0; i < this.leaders.length; i++) {
            if (i < 5)
                updates[`/${i + 1}`] = this.leaders[i];
            else
                updates[`/${i + 1}`] = {};
        }
        return this.ref.update(updates);
    } else {
        this.uploadInstruction.text = 'You have already submitted your score!';
    }
};

Game.reset = function() {
    if (!game.paused) {
        this.diffculty = DIFFCULTY.rookie;
        this.uploading = false;
        this.submitted = false;
        this.player.dataReset();
        this.enemies.dataReset();
        this.explosions.dataReset();
        this.emitters.dataReset();
        this.items.dataReset();
        this.resetUI();
        this.removeGameOverLabel();
    }
};

Game.resetUI = function() {
    this.score = 0;
    this.scoreLabel.text = `Score: ${this.score}`;
    this.healthLabel.text = `Health: ${this.player.health}`;

    this.energy.ratio = (Game.player.ultimateCount * Game.player.ultimateCooling + Game.player.ultimateCD * !Game.player.ultimateCooling) / Game.player.ultimateCD;
    this.energy.scale.x = Game.energy.ratio;
    this.energyLabel.flash.stop();
    Game.energyLabel.visible = true;
    this.energyLabel.alpha = 1;
    this.energyLabel.flash = game.add.tween(this.energyLabel).to({alpha: 0}, 400).yoyo(true).loop(true).start();
};

Game.toggleUI = function() {
    if (this.rightArrow.visible) {
        this.UITween(40, 500);
        this.leftArrow.tween.onComplete.add(this.toggleArrow, this);
    } else {
        this.UITween(-15, 500);
        this.leftArrow.tween.onComplete.add(this.toggleArrow, this);
        this.setVolumeControlVisibility(false);
    }
};

Game.UITween = function(position, duration) {
    let buttonPosition = position - 5;
    let arrowPosition = position + 25;
    this.curtain.tween = game.add.tween(this.curtain).to({x: position}, duration).start();
    this.homeButton.tween = game.add.tween(this.homeButton).to({x: buttonPosition}, duration).start();
    this.optionButton.tween = game.add.tween(this.optionButton).to({x: buttonPosition}, duration).start();
    this.restartButton.tween = game.add.tween(this.restartButton).to({x: buttonPosition}, duration).start();
    this.pauseButton.tween = game.add.tween(this.pauseButton).to({x: buttonPosition}, duration).start();
    this.soundButton.tween = game.add.tween(this.soundButton).to({x: buttonPosition}, duration).start();
    this.muteButton.tween = game.add.tween(this.muteButton).to({x: buttonPosition}, duration).start();
    this.rightArrow.tween = game.add.tween(this.rightArrow).to({x: arrowPosition}, duration).start();
    this.leftArrow.tween = game.add.tween(this.leftArrow).to({x: arrowPosition}, duration).start();
};

Game.toggleArrow = function() {
    this.rightArrow.visible = !this.rightArrow.visible;
    this.leftArrow.visible = !this.leftArrow.visible;
};

Game.setVolumeControlVisibility = function(visibility) {
    if (!game.paused) {
        this.soundCurtain.visible = visibility;
        this.mainicon.visible = visibility;
        this.mainVolumeBar.visible = visibility;
        this.mainVolume.visible = visibility;
        this.bgmicon.visible = visibility;
        this.bgmVolumeBar.visible = visibility;
        this.bgmVolume.visible = visibility;
        this.effecticon.visible = visibility;
        this.effectVolumeBar.visible = visibility;
        this.effectVolume.visible = visibility;
    }
};

Game.pauseGame = function() {
    if (!this.uploading) {
        if (!game.paused) {
            this.mask = game.add.graphics(0, 0);
            this.mask.beginFill(0x000000, 0.3);
            this.mask.drawRect(0, 0, game.width, game.height);
            this.pauseLabel = initLabels(game.width / 2, game.height / 2, 'PAUSE', {font: '50px Arial', fill: '#ffffff'}, 1.0, 1.0);
        } else {
            this.mask.destroy();
            this.pauseLabel.destroy();
        }
        game.paused = !game.paused;
        this.mainVolume.inputEnabled = !this.mainVolume.inputEnabled;
        this.bgmVolume.inputEnabled = !this.bgmVolume.inputEnabled;
        this.effectVolume.inputEnabled = !this.effectVolume.inputEnabled;
    }
};

Game.muteSound = function() {
    if (!game.paused) {
        this.onMute(false);
        this.soundButton.visible = !game.global.backgroundMusic.mute;
        this.muteButton.visible = game.global.backgroundMusic.mute;
    }
};

Game.onMute = function(forced, mute) {
    if (forced) {
        game.global.backgroundMusic.mute = mute;
        this.player.attackSound.mute = mute;
        this.player.explosionSound.mute = mute;
        this.enemies.attackSound.mute = mute;
        this.enemies.explosionSound.mute = mute;
        this.alarm.mute = mute;
        this.launch.mute = mute;
        this.upgrade.mute = mute;
    } else {
        game.global.backgroundMusic.mute = !game.global.backgroundMusic.mute;
        this.player.attackSound.mute = !this.player.attackSound.mute;
        this.player.explosionSound.mute = !this.player.explosionSound.mute;
        this.enemies.attackSound.mute = !this.enemies.attackSound.mute;
        this.enemies.explosionSound.mute = !this.enemies.explosionSound.mute;
        this.alarm.mute = !this.alarm.mute;
        this.launch.mute = !this.launch.mute;
        this.upgrade.mute = !this.upgrade.mute;
    }
};

Game.setEffectVolume = function(volume) {
    this.player.attackSound.volume = volume;
    this.player.explosionSound.volume = volume;
    this.enemies.attackSound.volume = volume;
    this.enemies.explosionSound.volume = volume;
    this.alarm.volume = volume;
    this.launch.volume = volume;
    this.upgrade.volume = volume;
};

Game.adjustVolume = function(slider) {
    switch (slider) {
        case this.mainVolume:
            game.sound.volume = (slider.x - slider.minX) / (slider.maxX - slider.minX);
            break;
        case this.bgmVolume:
            game.global.backgroundMusic.volume = (slider.x - slider.minX) / (slider.maxX - slider.minX);
            break;
        case this.effectVolume:
            game.global.effectVolume = (slider.x - slider.minX) / (slider.maxX - slider.minX);
            this.setEffectVolume(game.global.effectVolume);
            break;
        default:
            break;
    }
};

function bulletPoolSearchRange(weaponTag) {
    if (weaponTag === WEAPONS.ammo.tag)
        return {start: 0, end: 100};
    else if (weaponTag === WEAPONS.burst.tag)
        return {start: 100, end: 200};
    else if (weaponTag === WEAPONS.superhot.tag)
        return {start: 200, end: 300};
    else if (weaponTag === WEAPONS.bomb.tag)
        return {start: 300, end: 400};
    else
        return {start: 0, end: 400};
}

function bulletTrace(bullet) {
    let targetAngle = game.math.angleBetween(bullet.x, bullet.y, bullet.target.x, bullet.target.y);
    if (bullet.rotation !== targetAngle) {
        let delta = targetAngle - bullet.rotation;
        if (delta > Math.PI)    delta -= Math.PI * 2;
        if (delta < -Math.PI)   delta += Math.PI * 2;
        bullet.angle += delta / Math.abs(delta) * bullet.turnRate;
        if (Math.abs(delta) < game.math.degToRad(bullet.turnRate))
            bullet.rotation = targetAngle;
        bullet.body.velocity.x = Math.cos(bullet.rotation) * bullet.speed;
        bullet.body.velocity.y = Math.sin(bullet.rotation) * bullet.speed;
    }
}